# Test data set

This is a data set to test the `PathoFact` pipeline.

See `README.md` in parent directory for `PathoFact` set-up instructions.


## Test run

Include the required path to the SingalP v5.0 installation to the config file

```
# activate env
conda activate PathoFact
# run the pipeline
snakemake -s test/Snakefile --use-conda --reason --cores 1 -p
```
