#!/bin/bash -l

conda activate PathoFact

snakemake -s Snakefile --use-conda --rerun-incomplete --cores 28 -p
