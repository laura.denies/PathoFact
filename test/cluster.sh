#!/bin/bash -l

#SBATCH -J PathoFact
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --time=2-00:00:00
#SBATCH -p batch
#SBATCH --qos=qos-batch

# activate env
conda activate PathoFact

# run pipeline
snakemake -s test/Snakefile \
--use-conda --cores 10 --reason -p \
--cluster-config cluster.yaml --cluster \
"{cluster.call} {cluster.partition} {cluster.quality} {cluster.nodes} \
{cluster.runtime}{params.runtime} {cluster.mem_per_cpu}{params.mem} \
{cluster.threads}{threads} {cluster.job-name} {cluster.output}"
